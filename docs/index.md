# Home

![](./images/week02/tnv1_7aa.jpg)  
*3D model of the Not-in-a-hurry-pocketwatch*

## Welcome to my Fab Lab 2020-2021 project site

In here you can find my adventure through Fab Lab courses between 2020 and 2021, culminating into my final project,

**The Not-in-a-hurry-pocketwatch.**  
(Actual name still in the works)
