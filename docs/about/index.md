# About me

![](../images/avatar.jpg "Me on our new beanbag chair")

Hello! I am Eemil Tryyki. I am an electrical engineering student in University of Oulu, in Finland.

Visit this website to see my work!

## My background

I originate from Tornio, which is a medium-sized (by Finnish standard) town at the Swedish border in Southern Lappland. 2006 I moved to study in Oulu and I'm still on that journey to knowledge. Thanks to my father, I have alot of interest in Do-It-Yourself -culture, ranging from 'simple thing that I could buy, but I want to do it myself' to 'I can apply this thing to do this job instead'.

## Previous work

I've most of my professional career has been summerjobs, for example an electrician-apprentice for Outokumpu Stainless, measurement-technician for Destia, and recently Research Assistant for FabLab-project.

## Contact

Emails:  
firstname.lastname(at)student.oulu.fi  
eeepista(at)hotmail.com

Social: (Eemil Tryyki)
Facebook
Linkedin
