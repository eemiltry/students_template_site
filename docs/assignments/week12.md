# 12. Output devices

assignment
   individual assignment:
      add an output device to a microcontroller board you've designed,
         and program it to do something
   group assignment:
      measure the power consumption of an output device

---
TODO: add code, edit and add video1, possbly remove audio from video2  
---

## Design parameters

By tutors advice, I made both input and output device on one PCB, having their own connectors.

As I'm thinking of using Neopixels in my final project, I decided to make my output-device with 2 Neopixels, their data-input-resistor of 499 ohms, filtering-capacitor of 1uF and 6 pin connector FTDI-style.

![](../images/week12/debug0.png)  
*KiCAD view of my PCB layout*

This design had a bit of a __*leFü*__ -moment as it heated up as soon as power was connected, so I had to debug it.

## Debugging output-circuit

With my input/output -board, there began a debugging process, as output-side was overheating instantly after connecting, so some sort of short circuit was there, but where? Bad soldering?

After inspecting and re-soldering everything under microscope, I probed the connections, everything seemed to be in order, it still began heating after connecting power, so something was wrong.

This time I decided to remove Neopixels from the board, and thus I happened to tear one of the copper pads, so I had to make a new board. When I reviewed my design in KiCAD, I decided to doublecheck all components and their footprints before making new board.

![](../images/week12/debug0.png)  
*Neopixel_THT pins: 2 GND and 3 VDD*

Wellwellwell, would you look at that?

![](../images/week12/debug3.png)  
*SK6812 vs. Neopixel_THT (=WS2812), they have different pin order*

My schematic and layout had WS2812-symbol/footprint (with name Neopixel_THT) and I have __RGBW__ -neopixels,  which are actually SK6812, soldered on to my board. So, I was actually connecting my VCC to GND-pin of the pixels. No wonder they heated up.

### Why did this happen?

Well, both types have mostly identical footprints for layout as seen in below images, and as first time user, I thought they have same pin order as they have same casing/footprint.

Second problem was 'just quickly checking datasheet (which happened to be the right one despite using wrong one in layout) on which pin has that corner mark on', which is *pin 1* (as seen below), but because I'm used to __GND__, I just saw VSS as different marking for power like VDD, VCC etc.

![](../images/week12/datasheet.png)  
*SK6812 Pin Map*

So that's why I made a mistake, and most likely burned out both Neopixels.

![](../images/week12/debug1.png)  
*Neopixel_THT -footprint is WS2812B*

![](../images/week12/debug2.png)  
*SK6812 -footprint is the same as in WS2812B*

So, time to swap some LEDs and make new routes.

![](../images/week12/new1.png)  
*Layout 1.1, using SK6812*

Added fill-zone for copper and back to LPKF-router to make the board.

## Board 1.1

After soldering components on my new board, it was time to test if it will work. As with Input-device, I have to make this work through ATtiny412-echoboard via FT230XS-programmer. So amount of usable pins might become a problem.

![](../images/week12/attiny412.jpg)  
*Using ATtiny412-echo-board as my microcontroller for output device*

I decided to test those Neopixels that had been connected to my faulty board before. So I soldered them on my board 1.1 and this time they were connected right.

BUT: the first inline from Vdd (closer to connector) was damaged as it only produced red light, the other one atleast had it's other LEDs working. Too bad I forgot to take a video of it, as I can't remember if second RGB changed colour only when first one could use it's red.

![](../images/week12/test1a.jpg)  
*ATtiny412 connected to outputdevice, yellow as GND, red as Vdd, orange as TX, brown as RX*


__CODE HERE__

![](../images/week12/test1c.jpg)  
*RGB1 lights up*

![](../images/week12/test1b.jpg)  
*RGB2 lights up*

__VIDEO1 HERE__

Next I replaced Neopixels with new ones (SK6812-type), using heatgun to remove old ones from the board to avoid tearing copperpads.

Blink worked fine, then I run __CODE HERE__ to test colours. It was hard to take proper pictures, so a video is included below these pictures.

![](../images/week12/test2a.jpg)  
*RGB1 bright blue with RGB2 fading*

![](../images/week12/test2b.jpg)  
*RGB2 pink*

![](../images/week12/test2c.jpg)  
*RGB2 bright green*


<figure class="video_container">
  <video width="400" height="400" controls="true" allowfullscreen="true" autoscale="true" poster="docs/images/week12/test2a.jpg">
    <source src="https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/videos/RGBtest2.mp4" type="video/mp4">
  </video>
</figure>

---

__OLD__ RGB-LED

![](../images/week12/output0.jpg)  
*RGB-output board pre-outlinecutting*

![](../images/week12/output1.jpg)  
*output board ready for components*

![](../images/week11/output2.jpg)  
*Board ready*
