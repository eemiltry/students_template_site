# 7. Electronics design

assignment
   group project:
      use the test equipment in your lab to observe the operation
         of a microcontroller circuit board
   individual project:
      redraw an echo hello-world board,
      add (at least) a button and LED (with current-limiting resistor)
      check the design rules, make it, and test it
      extra credit: simulate its operation

## KiCAD



## EAGLE

This week we had "piano lesson" for Eagle PCB -program usage to design our PCBs. I learned that the problem, where pngs were exported 2x size was probably that I was missing FabLab library in Eagle.
Anyways, we had to design our USB-programmer circuit, the one that I had already milled, from scratch.

After Eagle installation, you should go to Fabacademy.org-> content-> 2020-> schedule-> electronics design-> libraries-> eagle-> fab.lbr-> download to your chosen libraries folder.

Now in Eagle Control Panel: options-tab-> directories... add your chosen libraries directory after Eagle's own libraries. Time to make a new project, so let's call it for example as UDPetc.proj.

Next I made a new schematic for the project and started adding parts onto it. Most of them were from Fab-libraries: micro-controller FT232RL, capacitor C1206FAB, resistor R1206FAB, connector CONN_06_FTDI-SMD-HEADER, USB-connector CONN_USBPCB, ground GND, voltage VCC. As our PCBs were only 1-sided, we chose FAB-versions, which were a bit wider in size so we could route our wiring beneath them, as our resistor and capacitor. The parts were arranged in somewhat logical order, as this layout was functional view of the circuit, and relevant connectors were connected using Draw an electrical connection -tool. Resistor was valued as 4.99kOhm and set between TXD and RXD connectionwires. Capacitor was valued as 1uF and set between VCC and GND, with +side pointing to VCC. Every GND-pin on the components and TEST-pin on microcontroller were connected to GND, every VCC-pin connected to VCC and VCCIO, RESET, 3V3OUT were connected together.

After all this was done, it was time to move to Board-design by using SCH|BRD -button. Now in this view we can layout the physical components and their do their physical wiring. First I had to decide whether to do landscape- or portrait-design. As it was to be an USB-stick -shaped, it would take much more space if done in portrait-form, and Antti recommended not to do it like he had originally made it as it would mean lot more scrolling around than landscape-form, so I chose to do it in landscape. Now I had to drag and drop the components to their approximated locations and after that do "the undoing of ratnests" aka. use 'Calculate the shortest airwiring'-button. Now the airwires weren't that bad to follow.

First we had to decided how big was the finished board to be. As USB-connector is 12mm wide, that was a deciding factor for size, so we chose layer46 (Milling) as our cutting job and drew lines around our board design. Next we configured Design Rules Check (DRC) clearances to 8mm in every category, chose wire width to 16mils. Then it was time to do the wiring of the PCB as layer 1(Top).

Wiring was done quite easily as I was familiar with CAD-style programs, Eagle itself and I had a milled this board last week.

After wiring was done and no errors were detected by DRC, it was time to export .png of our top-layer and milling-layer. This was done by selecting top or milling layer as the only visible layer and then file->export->image.


"BIGPROGRAMMER":
Antti innovated an advanced programmer that we should design and manufacture.
Components: FT232RL, ATMEGA328P, 3x2 AVRSPSMD-connector, resistors (2x 0, 1k, 10k, 4,99k), capacitors (2x 1u, 100n ), 16MHz resonator, green SMD-LED, 1x3 RA female connector.
Pictures

As seen, it will have USB-A-connector.

It was recommended to use calibration markers on edges of the board to help in adjusting the milling machine and to spot if the board was bended to some direction.

My first version wasn't a complete failure and it taugth me to increse my DRC clearances to 9mils to make problems easier to spot when milling with 0.2-0.5 tool. Also it was important to clear the sacrificial layer of any loose particles before adhesing anything on it and check if the layer was loose (it is possible to do minute-adjustment by loosening/tightening the holding screws).

1st versions mods-setup for mill-tracing was 0.23mm tool, 0.1016 depth, 6 offset-outlines, 0.8 offset-diameter. I didn't systematically check the milling-routes, only the problem-zones around FT232L and ATMEGA328P -pads, so there were few places where lines were not separated and some places were left connected.
I tried to do some revision milling to fix these problems, first by changing some angles of my wiring, this fixed some of the issues, but thinned one wire quite a lot as design was moved a bit too much. Next I did another revision in Eagle by drawing some new lines (on different layer) between wires that were not milled properly. I exported  inversed image of these lines to mods, and iterated configuration some time until I found usable parameters. I used millcutline as basis, changed tool diameter to 0.10mm and depth to same as tracing, 0.1016mm, and calculated milling-routes that would mill out only my revision-lines. This worked quite well, but I had to leave and so I forgot to cut outlines of my design while it was still adhesed inplace.
So next day, I had to either use a scalpel to finish wiring and the manually cut my design out of the board or mill a new one. I chose the latter and began changing my design to prevent those faults of the last version. Changing DRC clearances to 9mils was the first job, then re-routed some wiring to fit the milling paths better.
This time I decided to use some of the leftover-space on my board to fit more calibration markers outside and inside the design. I exported images of my wiring-, calibration- and outline-layers to mods. For tracing I used 0.22mm tool diameter, 0.1016mm depth, 6 offset-outlines, 0,5 offset-diameter, and I used it for both wiring and calibration. Milling the cut line, I set 1mm tool, ~0.6mm depth per round, 1.7mm exact for maximum depth.

The milling of 2nd version:
As the 1st version had some particles caught under it and thus affecting it's plane, I made sure to clear the sacrificial layers surface thoroughly by using masking tape to catch any loose particles. After cleaning I adhesed my board onto middle of the sacrificial layer and then checked that the layer was tightly in place.
Next I set X/Y-origo to front left corner of of the board, and as per usual, lowered tool carefully close to surface of the board, set Z-origo and then gently lowered the tool-bit on surface by loosening and then tightening it.
Now I adjusted z +0.10mm from z-origo (the surface of the board) and run my calibration toolpaths 5 times until I was satisfied with the results and my markers were nice and clear all around the board. Z-axis was lowered, from z-origo +0.10mm, through these iterations by (-0.02-0.02-0.01-0.01)mm=-0.06mm. So my final z-axis was +0.04mm from surface (z-origo).

After milling the traces was done and inspected, it was found quite perfect, Antti even congratulated me of work-well-done. Next I changed the tool and run outline cutting to separate the circuit from the rest of the board.

Now I had to use steelwool to clear the lacquar from coppersurface so I could solder my components to it.
