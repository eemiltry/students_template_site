# 1. Principles and practices

## Have you?

    Read the Fab Charter
    Sketched your final project idea/s
    Described what it will do and who will use it

I read [Fab Lab Charter](http://fab.cba.mit.edu/about/charter/) and acknowledge it.

## Sketch of my final project idea

![](../images/week01/concept1.jpg)
![](../images/week01/taskunauris_sketch2.jpg)

As shown in sketches above, my final project would be a digital pocket watch, that shows time with 5minute accuracy, e.g. "it's five-ish" or "it's close to half past something"

So some design parameters for the watch:

  -pocket sized, max. 8cm diameter

  -chassis of milled aluminum, housing the electronics

  -front cover as lid, made from cast-aluminum or -brass

  -back cover of smooth red stone, rough-cut with waterjetcutter then polished by hand

  -clockface of lasercut thin-plywood, with slits for LEDs, possibly something rastered

  -electronics design marks the rest of the dimensions, how small can the chassis be to fit everything in

    -12 RGB/twotone -LEDs or 24 LEDs ?

    -bluetooth-connection to smartphone for time checking/adjustment

    -LEDs light up when lid is opened, with phototransistor or something?

So that's the initial plan, we'll see if something needs rethinking.

## What/who is it for?

It is a personal accessory item.

It could have potential as novelty accessory item for people to make themselves or (those who don't want to or can't) to buy custom-made. It's possible to market it as a downshifting product to lower stress and hurry that people feel

## Gallery

![](../images/week01/concept1.jpg)
![](../images/week01/taskunauris_sketch2.jpg)
