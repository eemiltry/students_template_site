# 5. Electronics production

assignment
   group assignment:
      characterize the design rules for your PCB production process
   individual assignment:
      make an in-circuit programmer that includes a microcontroller
      by milling and stuffing the PCB, test it,
      then optionally try other PCB processes

As my older ATtiny45 -devices were not that compatible with my new FT230XS-based programmer, (and I seemed to have miss-placed my screenshots and photos of them somewhere) and I was interested about using LPKF __CHECK-MODELTYPE__ for PCB production. And, *oh boy*, it is magnificent compared to ROLAND SRM-20 & http://mods.cba.mit.edu/ -combo.

## Using LPKF XYZ

__*LPKF CircuitPRO PM 2.7*__

![](../images/week11/lpkf0.png)  
*Startup of the program*

After startup, I used *Process planning wizard* (first icon from left on "CAM Machining"-toolbar, the one with a wand)

![](../images/week11/lpkf1_ppw.png)  
*Process planning wizard*

Now with the wizard we have a easy continuous process of setting up our production.

First we select *Process PCBs*.

![](../images/week11/lpkf2_ppw.png)  
*Next we select our layer layout as Single-sided top*

![](../images/week11/lpkf3_ppw.png)  
*Selecting substrate-type*

Our tutor *Antti Mäntyniemi* recommended to use FR4/FR5-settings (fiberglass-substrate) even if our boards are FR2 (cardboardlike-substrate) so the machine would work a bit slower, thus less stress to machine, I guess.

After *PPW* (*Process planning wizard*) we can import our design by selecting the next icon on "CAM Machining"-toolbar and selecting our *gerber* files of our design.

![](../images/week11/lpkf4_import.png)  
*Import window, selecting both Edge Cuts and Copperlayer*

![](../images/week11/lpkf5_import.png)  
*Checking that program interpret layers correctly, F_CU should be TopLayer and Edge_Cuts is BoardOutline*

Need to generate toolpaths for our design. so we select toolpath icon right of crosshair-icon __(CHECK_UP_IF_CORRECT)__ on "CAM Machining" -toolbar

![](../images/week11/lpkf6_toolpath.png)  
*Isolate -TAB: Select 4th process 'Complete rubout' to remove all excess copper in design*

![](../images/week11/lpkf7_toolpath.png)  
*Contour routing -TAB: tutor advised on using 2nd setup 'Horizontal gaps' for ease of machining and board removal*

Now that we have our toolpaths ready, we go to *Board Production Wizard* (*BPW*) by selecting 2nd icon from right on "CAM Machining" -toolbar. (again, the icon has a wand)

![](../images/week11/lpkf8_bpw.png)  
*Board Production Wizard, mount the material into the machine using adhesive tape*

So, we mount our FR2-board onto the machines vacuumtable with markertape holding the edges.
__Important note:__ be mindful of the fact that the tool has black support collar that's about 13mm from tool and it presses on the board when tool is used, and that collar should not go over the edges of the board or on the adhesive tape.

Next the program asks you select your working area on the material (front left corner as P1 and back right corner as P2)

__SCREENSHOT OF WORKING AREA SELECTION__

![](../images/week11/lpkf9_placement.png)  
*Drag your design on the working area, default settings will suffice*

In next phase the program wants to calibrate machine by milling a specified line, select a place in work area where to do that line, I made my line on to the empty area on top right of my design so it won't waste any extra material on work area.

![](../images/week11/lpkf10_calib.png)  
*Machine asking calibration, chose line to be made on unused area in my design*

After placing calibration line on work area, select *Mill a Line*, machine mills a line and then tries to focus on it's camera on the calibration line. If camera view stays blurry, use *autofocus* above camera view, if it still not focusing, try making another calibration line elsewhere on the working area.

When camera is focused on the calibration line, program gives a line width that is way-off (tutor has contacted the manufacturer about this), so we need to manually measure the calibration line.

![](../images/week11/lpkf11_measure.png)  
*Right click on camera view to select point to point measuring mode*

Then measure *width* of the line in camera view, it should be *around 0.2mm* (mine was 0.282, but it's close enough cause of clicking points into camera view)

After accepting width the milling begins. Even with tiny text on the design, the milling process was blazingly fast compared to Roland SRM-20.

After lines are done, the machine switches to different tool for removing excess copper, and again it asks you to select calibration line. __However__ the program shows it as a line, it will be a *1mm diameter circle*, so place it carefully.

After removal of excess copper is finished, it's time for BoardOutline-job. Machine will drill pilot holes near *the Horizontal Gaps* and then with different tool, mill the board outline.

After all is finished, it's well mannered to clean after your usage. After removing your board, sweep dust from machine and check if tool is still in the collar. If so, select 3rd from right icon (*edit Tool Magazine*) on "CAM Machining" -toolbar, it should open *Tool Magazine* -window and from there, click the crosshair to return the tool to magazine.

![](../images/week11/lpkf12_finish.png)  
*Return tool to magazine by clicking crosshair next to tool, here it's next to 'Contour router'*

![](../images/week11/inputoutput.jpg)  
*TA-DAA!*

![](../images/week11/inputoutput2.jpg)  
*Components soldered + jumpwire*

__NOTE:__ When I was soldering the components on board, I noticed a mistake within my lines, that I forgot to have a straight line from collector to out-pin. It might still work, but just in case, I soldered a jump wire for it. I'll check this out and remove this note if it works as intented.

---

__OLD:__

__Note:__ this document follows my 2020 version of a programmer.

As individual work we had to produce a programmer for future use. As I arrived to do that on Friday 24.1.2020 Antti intercepted me and asked if I wanted to tryout doing his new layout.

So I began doing so by trying to export .png out of Eagle to 'mods', but there seemed to be a feature in my Eagle setup that caused the image to be 2x sized to original, so milling parameters were difficult to configure for Roland SRM-20. So it's left to be troubleshot, but we managed to produce working .rlm to run in Roland. Now to the milling.

First we cleaned sacrificial board, then adhesed a piece of FR1 onto lower left corner of sacrificial board. Then I installed a V-shaped 0.02-0.50 -cutting tool and then calibrated the axes, by doing calibration marks first.



![](../images/week05_elecprod/calibration3.jpg)  
*Calibration marks in the works*

![](../images/week05_elecprod/calibration2.jpg)  
*Different board, but better image of marks: right-most marks still have plenty of copper around traces*

We setup Rolands x/y -origins as drawn, but for the first run we set z-origin as +0.20mm. First run cut a bit too deep as you can see on microcontrollers pads being too narrow. So Z-axis needed +0.05mm adjustment for the second circuit. I just adjusted the x-origin +15mm to fit another circuit on the same piece of board.

![](../images/week05_elecprod/calibration3.jpg)  
*Calibration marks in the works*

Second run went very well, except board was warped in the lower right corner, so lower part of circuit didn't get milled, as seen in the picture. So I scrounged for a new piece of board from the leftovers, found one suitable and cut a piece from it to fit two new milling-trials.

![](../images/week05_elecprod/calibration3.jpg)  
*Calibration marks in the works*

Third run, after aiming x/y-origin to new workpiece, I began with z-origin +0.20mm above the workpiece, but the tool only scratched the copper, so I lowered z-origin to +0.10mm. Now the milling worked like a charm, and while the workpiece was still firmly stuck on the sacrificial board and a fellow course-participator arrived, I decided to showcase the process and mill a fourth piece next to third piece by just changing y-origin by +47mm (height of the layout).

![](../images/week05_elecprod/calibration3.jpg)  
*Calibration marks in the works*

After fourth circuit was done and inspected, I run outline cutting on these 3rd and 4th circuits. For cutting the outlines, the tool had to be changed to a regular milling bit (and z-axis calibrated to surface of the workpiece) and milling file changed to cutting file. 4th circuit was cut first as it's x/y-origin was still set and to cut the 3rd circuit, only y-origin needed changing by -47mm.

![](../images/week05_elecprod/cut1.jpg)  
*3rd board outline cut, still adhered to tape with rest of the FR1-blank*

After removing workpieces from the milling machine and cleaning up after myself, the circuits had some leftover copper (around USB-connectors and back of the piece)that needed to be removed with a sharp knife.
