# 13. Applications and implications

##    Your project should incorporate:

        2D and 3D design

        additive and subtractive fabrication processes

        electronics design and production

        embedded microcontroller interfacing and programming

        system integration and packaging

      Where possible, you should make rather than buy the parts of your project

__TODO:__ Add pictures of concept, add pictures for materials

  My final project masterpiece would be __*a digital pocket watch.*__

## __What will it do?__

  - uses analogue clock-face to tell time approximately, about 5min accuracy, communicates with smartphone to synchronize time

  ![](../images/week02/concept1.jpg)

  ![](../images/week02/concept2.jpg)

  ![](../images/week02/tnv1_7aa.jpg)

  ![](../images/week02/tnv1_7bb.jpg)

  ![](../images/week02/tnv1_7cc.jpg)

---

## __Who's done what beforehand?__

I don't have access to FabLab archive of final-project, but doing a quick search, I didn't find complete watches been made.  

*I will recheck this*  

---        
## __What will you design?__

   - overall aestethics, electronics and mechanics

   - coding of 5 minute accuracy

---
## __What materials and components will be used?__

   - backcover: polished stone (waterjetcut, hand finished) *or just lasercut plywood as backup.*

   - frontcover: cast metal, aluminum or brass (molding and casting)

   - chassis: aluminum (CNC) *or possibly cast*

   - clockface: lasercut/-rastered plywood

   - electronics inside chassis:

  - 12 single-input-RGB-LEDs (Neopixel SK6812)

    - xxxxxx -microcontroller *still comparing which one*

    - X filter-capacitors

    - Y input-resistors

- WorkInProgress

---

## __Where will come from?__

  - components: FabLab inventory

  - stone: my storage __(add picture)__

  - aluminum for casting: my storage, old cooling-fins?

  - aluminum for CNC: my storage, if one of my cooling-blocks is suitable?, if not, then other source

---         
## __How much will they cost?__

(approximated from fablab inventory)

RGB-LEDs: https://www.amazon.com/Aclorol-Individually-Addressable-Programmable-Non-Waterproof/dp/B07BGSZLGX	WS2812B LED Strip Light 30 Pixels/M 16.4ft 5050 RGB SMD	19.90$	(19.90$/30 x12 = 7.96$)

filter-capacitors: ~0.07$ x X = 0.84$

input-resistors: ~0.01$ x Y = ?

microcontroller: atmel somethingsomething ~1$ *(or one with integrated radios like ESP32something)*

aluminum: (quick search for CNC material)
  AW-5754 -untreated surface
  5mm thick, 250x250mm ~28,90€


---         
## __What parts and systems will be made?__

  - stone for backcover, *should be rough-cut with waterjetcutter?*

---         
## __What processes will be used?__

  CNC for molds, chassis and PCB

  Casting for lid (*possibly for chassis too?*)

  Lasercutting for clockface

---  

## __What questions need to be answered?__

  - How much space electronics really need? LPKF helps making the PCBs smaller.

  - Can I make it smaller than 80mm diameter? 2-sided PCB or neopixels on their own PCB.

  - Smartphone or RealTimeClock for time synchronization?

---      

## __How will it be evaluated?__

  - if it's made, but not working correctly, __*ok*__
  - if it works once, __*fine*__
  - if it works most of the time, __*good*__
  - if it works like it should, __*great*__
  - if it works like I want it to work, __*outstanding*__

---
---
