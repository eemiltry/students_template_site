# 19. Invention, intellectual property and income

TODO:
    Outlined future possibilities and described how to make them probabilities

    Update as I get showable objects done

*This assignment was done in early phase of the whole project, so I will update it as I make the physical assets to display.*

## Future of project

My pocketwatch project started as making a personal accessory item to go along with my new waistcoat. It could have potential as novelty accessory item for people to make themselves or (those who don't want to or can't) to buy custom-made. It's possible to market it as a downshifting product to lower stress and hurry that people feel, more about it in my extra slide show [below](#extra), that I erroneously made instead of a presentation poster.

### Copyright license

I doubt that I'm making anything unique besides how to provide the time-information, so it would be a novelty. I think same thing could be made mechanically, minute hand just moves in 5 minute increments, instead of 1 minute at a time.

I think my coding goes under [MIT](https://opensource.org/licenses/mit-license.php/) or [GNU general public license](https://www.gnu.org/licenses/gpl-3.0.html)

[MIT license:](https://opensource.org/licenses/mit-license.php/)
```
"Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
```
So, yes, I think my coding will go under [MIT license:](https://opensource.org/licenses/mit-license.php/) as I think my coding will be very basic and not that innovative.

My 3D models and electronic-designs would need a different license as they are not software related, so I think would choose [Creative Commons](https://creativecommons.org/) and it would be allowing adaptations of my work as long as others share alike and not allowing commercial uses of my work. Also the base idea will go under this license.

#### My copyright license for this project:

Coding: [MIT license:](https://opensource.org/licenses/mit-license.php/)

3D models, designs and base idea:

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


*Licensing is still pending on what kind of and how much work this project demands for completion.*

### Commercial possibilities

I could showcase/market my finished project on social media platforms or [Etsy](https://www.etsy.com/) to find out if there is interest about this kind of product. "Old fashioned way" would be to make a few different variants of complete product and show them off at a tradeshow or similar.

If there is enough interest on this gadget, then some points need to be solved:

1. Manufacturing

  1. all parts build one by one takes time = higher price
    * more options of customisation?
  2. parts made as standardised
    * just assembly needed?
    * limited customisation options
    * something between those two above: standard innards and chassis, different style and look on the outside

2. Marketing

  1. Etsy or similar?

  2. Amazon?

  3. as business gift?

3. Capital

  * startup loan or similar


If chassis and electronics are mass-manufactured, then more time and resources are possible to use for customisation options, like different designs in cast-metal lid; different back covers from metal, wood, stone; different clockface designs, etc.



## The Not-a-Watchwatchers-Watch

Both presentation slide and video about the watch. __*(updating soon-ish)*__

### The NaWwWatch

![PNG of presentation](../files/presentation.png)


## Files

[PNG of watch presentation](../files/presentation.png)

[PDF of watch presentation](../files/presentation.pdf)

[SVG of watch presentation](../files/presentation.svg)

[MP4 of watch presentation](../presentation.mp4)
### Extra

At first I happened to make a whole slideshow as watch presentation, so as here it is as extra.

[PDF of watch presentation slides](../files/NaWwWatch.pdf)

[.odp of watch presentation](../files/NaWwWatch.odp)

## Video

__*Updating as I go along*__

---

*Timelapse of designing pocketwatch frame*  

<video controls width="400" height="400"
      loop muted preload="auto"
       poster="https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/presentation.png">
  <source src="https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/presentation.mp4" type="video/mp4">
  <p>Your browser doesn't support HTML video. Here is a <a href="(https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/presentation.mp4)">link to the video</a> instead.</p>
</video>
