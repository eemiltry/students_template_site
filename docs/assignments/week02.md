# 2. Project management

---
## Student agreement

[My agreement](../about/agreement.md)

---
##Summary:

I made my GitLab-account and forked students_template_site into my repository, editing it for my use. As I'm not that interested in making webpages, I decided to stay with template and use markdown and mkdocs.

---

## Getting started:

[GitLab](https://about.gitlab.com/)

I began using git as taught at our local lecture (video lectures), we setup our GitLab accounts by using Git Bash -commands.

First we create our account at [GitLab](https://gitlab.com/) and login. Then I manually *forked* "FabAcademy_oulu/students_template_site" into my repository to have all templates for making a FabAcademy repository. It included all week
assignments and folders for making a website with *Markdown* and *mkDocs*, more about them [below](#setting-up-my-webpages)

Fork:
  A fork is a personal copy of another user's repository that lives on your account. Forks allow you to freely make changes to a project without affecting the original upstream repository. You can also open a pull request in the upstream repository and keep your fork synced with the latest changes since both repositories are still connected.

### Workflow:

Next we tell Git who we are:

    git config --global user.name “your chosen name"

    git config --global user.email "your email address"

>   Here we are configurating git:
      The global level is used so as to set the value for the current operating system user.
      User is setup by giving it a name and an email address

Then generate a ssh-key for remote access:

    ssh-keygen -t rsa -b 4096 -C "username@student.oulu.fi"

Now we copy the public ssh-key (id_rsa.pub) and paste it to our GitLab user-Settings (can be found, for example, clicking your user tab in GitLab and from dropdown menu choosing preferences, SSH-keys -menu is located on sidebar, as seen in image below)

![](../images/week01/sshkeys.png)  
*Paste your ssh-key here and give it a name*

User-settings page has also instructions about ssh-key generating and using existing keys

Time initialize our own git local repository folder by making a clone of our remote repository:

    git clone url

      For example url for this repository https://gitlab.com/eemiltry/students_template_site.git  
      You can find it under *Clone* -tab on your remote repository

![](../images/week01/clone1.png)  
*Url under Clone -tab*

After cloning, we can move updates from remote to local:

    git pull origin master

Store changes that you have done in your local repo:

    git add  

    git commit -m “message”

Move updates from local repo to remote repo:

    git push origin master


#### List of basic Bash commands:
    git config - git configuration using variables
      The basic use of git config is creating it with a configuration name that displays the set value at that name.  
      Configuration names consist of a ''section'' and a ''key'' separated by a dot. More about it [here](https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)  
    git clone - creates the local copy of the remote repository
    git status - checking the changes made in your repository
    git pull - downloads and updates the files from the remote repository to local repository
    git add - adds the changed files ("git add -A" adds all the changed files)
    git commit -m 'comment' - commits the changes
    git push - updates the remote repository with the content of the local repository

### Some explanations:

#### Continuous methods: *CI/CD* = *Continuous Integration* / *Continuous Delivery* or *Continuous Deployment*

  [*Continuous Delivery*](https://continuousdelivery.com/) is a step beyond Continuous Integration. Not only is your application built and tested each time a code change is pushed to the codebase, the application is also deployed continuously. However, with continuous delivery, you trigger the deployments manually.

  [*Continuous Deployment*](https://www.airpair.com/continuous-deployment/posts/continuous-deployment-for-practical-people) is another step beyond Continuous Integration, similar to Continuous Delivery. The difference is that instead of deploying your application manually, you set it to be deployed automatically. Human intervention is not required

  [GitLab CI/CD -quick guide](https://docs.gitlab.com/ee/ci/quick_start/index.html)

![CI/CD flowchart](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_11_9.png)  
*CI/CD flowchart from GitLab documentation*

#### Pipeline:

Pipelines are the top-level component of continuous integration, delivery, and deployment. Pipelines comprise

  * Jobs, which define what to do. For example, jobs that compile or test code.  

  * Stages, which define when to run the jobs. For example, stages that run tests after stages that compile the code.

A typical pipeline might consist of four stages, executed in the following order:

  * A build stage, with a job called compile.

  * A test stage, with two jobs called test1 and test2.

  * A staging stage, with a job called deploy-to-stage.

  * A production stage, with a job called deploy-to-prod.

### Conclusion:
After few failed commits by mixing the command order, I decided to install [GitHub Desktop](https://desktop.github.com/) to help me. It provided me easy GraphicUserInterface that quickly shows what's changed and what I'm actually commiting to my repository.
Commit just needs *Summary* ( and optional *message*). It installs with guided setup and help to access your repo. More about this [here.](#how-to-upload-local-repository-to-remote-repository-using-github-desktop)

## Setting up my webpages

What are [markdown](https://www.markdownguide.org/) and [mkdocs](https://www.mkdocs.org/) that I used to create my project site?

[Markdown](https://www.markdownguide.org/) is a markup language that gives output in different formats and in my case the output is HTML.

[Mkdocs](https://www.mkdocs.org/) is a static site generator used by default in Fab academy to build project documentation, and Mkdocs converts the Markdown language to HTML in order to generate the websites.

[Interactive markdown tutorial](https://commonmark.org/help/tutorial/) is nice and easy way to get the hang of it, it takes about 10min to finish tutorial.

I decided to stay with [Mkdocs material -theme](https://squidfunk.github.io/mkdocs-material/) as it seemed to be right combination of simplicity and usability, both for writing and reading.

### Settings and configurations in repository

As I decided to do my webpages with Mkdocs, I had to edit my configuration-files:  

1. mkdocs.yml

  * Configuration file for markdown -site generator

  * Here I edited site name, description and author, and changed colour palette more to my liking.

![](../images/week01/mkdocs.png)  
*My mkdocs.yml -file*

2. .gitlab-ci.yml

  * Here is configured what python-build is used and contains the CI/CD configuration
    * Configurations you can define:
      * the scripts you want to run.
      * Other configuration files and templates you want to include.
      * Dependencies and caches.
      * The commands you want to run in sequence and those you want to run in parallel.
      * The location to deploy your application to.
      * Whether you want to run the scripts automatically or trigger any of them manually.

    * Here was the culprit of my [troubles](#troubleshooting)
      * my image was python.alpine, but alpine dropped gcc from build, so I changed it 3.8-slim.

### Examples of syntax

Use '*hashtags*' in front of a line to create topics, 1 for main-topic (here: 2. Project management), 2+ for sub-topics (here: Getting started and Setting up my webpages -> Examples of syntax). This will cause mkdocs to also create handy links to jump to different topics.

Use 2x'*tab*' in front of a line to create boxed sections, for example quotes etc.

    This is a quote.

Displaying row of raw code instead of it working, we use \ before code-syntax.  
Use '*' or '_' on both ends of text to make them italic and use them double to make them bold.

*italic* **bold**
> \*italic* \**bold**

*italic text with a __bold words__ within*
> \*italic text with a \_\_bold words__ within*

To show several lines of raw code, you can use 3x backticks (`) as parenthesis in beginning and end of said code. Here's the same 4 lines as above, using backticks.

```
  *italic* **bold**
  > \*italic* \**bold**

  *italic text with a __bold words__ within*
  > \*italic text with a \_\_bold words__ within*
```

Links are added using two different parenthesis, text in [] and link in ()

> \[Link name as text](https://www.your_url.here/)

Adding images in markdown is simple, same parenthesis-syntax as with links, but '!' added in front of the line.

> \!\[*text if image doesn't load*](../images/week02/example.jpg  "*Hold cursor over image -text*")


## Graphic tools for Git?

After having seen some of the tools for Git, I decided to use some form of GUI to better see what is happening. Installed [GitHub Desktop](https://desktop.github.com/) for git-handling and for webpages/documentation I chose [Atom](https://atom.io/).

GitHub Desktop shows immediately the changes in git repository as seen in screenshot of this file sometime before below. (green is new, red is deleted)

![](../images/week01/github2.png "GitHub Desktop showing changes in repository")  
*GitHub Desktop showing changes in repository*

Atom is a nice tool for doing this webpage with markdown. As shown in screenshot below, I used split-function to left and right side of screen as my 2K-display is nice for that kind of work. Repository shows leftmost, from where I can just pick files to show on my splits. I use left split for previewing images and files, and right split for markdown.

Atom also comes with Git-extension for straight up repository work with commit etc.

![](../images/week01/atom2.png "Atom showing work-in-progress of this page")  
*Atom showing work-in-progress of this page*

All I have to do, is edit my .md files in *Atom*, use *Markdown* -syntax and commit my changes to my remote repository.

### How to download/pull from remote repository using GitHub Desktop

![](../images/week01/fetch.png)  
*Use fetch origin button in the top, also when pushing, GitHub Desktop will automatically fetch origin*

###  How to upload local repository to remote repository using GitHub Desktop

![commit](../images/week01/commit.png)  
*Write summary (+ optional message) and press commit*

![push](../images/week01/push.png)  
*Press push, to automatically fetch origin (pull -command in git) from remote repository and then push your local repository to remote as commit*

![changes](../images/week01/changes.png)  
*Changes in local repository shown in the left, (red '-' =removed, green '+' =added, yellow 'dot' =changes in file) choosing an entry shows the changes in main window*

As such I find this much easier to view your workflow than with just using Bash-commands, as it had coloured clues for different actions, in a text file for example: addition is green, removal is red.

## Troubleshooting:

---

-had some problems with linking images in markdown, how to add all images in a folder to gallery?
  -tried to link my folder of images with image markdown syntax, but no avail:  
!![folder](../images/week01/)

```
![folder](../images/week01/)
```
  -so simply just linking a folder is not enough to bring all pictures from folder into gallery

  -haven't tried making a list from the pictures in a folder

---

-somesort of commit error, CI/CD error

-pipeline error, possibly caused by not having gcc installed (SOLVED: not in python.alpine anymore)

---

### Solutions:

#### CI/CD-error:  
  -as mentioned in [above](#settings-and-conficurations-in-repository), I solved my CI/CD errors in pipeline by switching to different python-build, that still had gcc in the build, as suggested by my tutor.

  -ci/co problems solved, thanks to my tutor's suggestion, by changing python from alpine to slim-3.8 and updating mkdocs.yml to same configuration as in current students_template_site (repository frame that I forked)

#### Image gallery:

-still trying to find solution, next try will possibly be as done [here, markdown gallery -script](https://github.com/leepenney/markdown-gallery)
