# 6. 3D Scanning and printing

This week we were shown different 3D-printers in our lab. Sindoh 3DWOX, Makerbot Replicator 2 and Leapfrog (Fab-rebuild) are our basic PLA-printers, Stratasys Fortus 380mc for ABS and other more demanding materials, and Formlabs for stereolitography printing.
We printed

![](../imaged/week04_3dprint/print0.jpg)  
*a*

![](../imaged/week04_3dprint/print1.jpg)  
*a*

![](../imaged/week04_3dprint/print1a.jpg)  
*a*

![](../imaged/week04_3dprint/print2.jpg)  
*a*

![](../imaged/week04_3dprint/print2a.jpg)  
*a*

![](../imaged/week04_3dprint/print1v2.jpg)  
*a*

![](../imaged/week04_3dprint/print1v2a.jpg)  
*a*

![](../imaged/week04_3dprint/print3.jpg)  
*a*

![](../imaged/week04_3dprint/print3a.jpg)  
*a*

![](../imaged/week04_3dprint/print3b.jpg)  
*a*

![](../imaged/week04_3dprint/print3c.jpg)  
*a*
