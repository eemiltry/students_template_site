# 9. Embedded programming

assignment
   individual assignment:
      read a microcontroller data sheet
      program your board to do something,
         with as many different programming languages
         and programming environments as possible
   group assignment:
      compare the performance and development workflows
         for other architectures

[ATtiny212/412](http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny212-412-DataSheet-DS40001911B.pdf)

[ATtiny212/412-diagram](https://www.mouser.com/images/marketingid/2018/microsites/112516496/Microchip_ATtiny212_412_Diagram.png)

[Spence Konde's ATtinyX12 -pins](https://raw.githubusercontent.com/SpenceKonde/megaTinyCore/master/megaavr/extras/ATtiny_x12.gif)

MEMO:
Win10 needs proper drivers for FTDI-chips to identify as COM-ports for Arduino IDE to use them:
http://www.ftdichip.com/Drivers/VCP.htm

Troubleshooting continues from input/output devices...

## Group work

Compare different MCUs

Done in group with Aleksi, Joonas and Toni.
