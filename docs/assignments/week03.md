# 3. Computer Aided design

## Shortcuts to different CADs

[Inkscape](https://inkscape.org/) is nice vector graphics editor. I've done plenty of different projects with it, from canvas-patch -designs to many different posters.

[GIMP](https://www.gimp.org/) is opensource 'photoshop', UI-layouts require a bit of getting used to, but otherwise very nice graphics editor.

[Fusion360](https://www.autodesk.com/products/fusion-360/overview) is powerful combination of different CAD-tools (CAD, CAM, CAE, and PCB software), but it's commercial, though Autodesk offers free education licence for students. From my point of view, it's more like a cloud-based service than individual program, as it defaults to cloudsyncing your projects and files, so it's hard to get access to the files if you can't use the program anymore, but it also allows you to continue your work from a different workstation that has Fusion360 in it.

[FreeCAD](https://www.freecadweb.org/) is powerful tool, but still not that easy to grasp for beginners, though it's better than it was a couple of years ago. Similar commercial one would be Autodesk Inventor.

![](../images/week02/freecad2.png)  
*FreeCAD with my lid-design*

[KiCAD](https://www.kicad.org/) is a cross platform and open source electronics design automation suite, providing open source alternative for Autodesk Eagle, a well-known electronics design CAD. It comes with a useful feature of having 3D-viewer build-in, so you are able to quickly check component layout for physical problems, like for example having too small area to join connectors properly. More about this in my [electronics design](https://eemiltry.gitlab.io/students_template_site/assignments/week05/) week when I've finished it's documentation.

![](../images/week02/kicad1a.png)  
*KiCAD Schematic-design*

![](../images/week02/kicad2a.png)  
*KiCAD Layout-design*

![](../images/week02/kicad3a.png)  
*KiCAD 3D-image of design*

[Inventor](https://www.autodesk.com/products/inventor/overview) is another product from Autodesk and it also has education license available for students. Not as versatile or easy to use as Fusion360, but it's still powerful. Instead of mostly based in cloud -type of program, it's still oldschool and works like a program, that loads and saves on your computer, so it's better for people using just one PC.

![](../images/week02/inventor2.png)  
*Rocking chair sled-feet design in Inventor*

---

##Job0: 2D raster of final project

  4 0 X : job not found

I just prefer doing vector images. So here's a quick guide to inverting layout image of an electronics schematic using *GIMP*.

### Job0.5 How to invert B&W image with GIMP

Open your layout image in [GIMP](https://www.gimp.org/).

![](../images/week02/invert1.jpg)  
*A schematic in GIMP*

Open *Colors* -dropdown-menu, select *Invert*

![](../images/week02/invert2.jpg)  
*Colors menu -> invert*

![](../images/week02/invert3.jpg)  
*Now layout colors are inverted*

And this easy is inverting black and white images in *GIMP 2.10.22*.  
Also if for example you have these layout images already and ready to go and use them and just then notice that some line is too close to other line to machine it properly, you can use *GIMP* to modify said line to give enough clearance.

#### Quick example of layout modification with GIMP:

"We are good to go, but lo an' behold, this pad is connected to wrong pad." So we need to switch that tiny bridge between component's footpads to other side.

![](../images/week02/GIMPmod1a.png)  
*Tiny bridge between 3rd and 4th footpad from left*

![](../images/week02/GIMPmod2a.png)  
*Use rectangle selecting tool to make a matching rectangle over that tiny bridge, and make a copy of rectangle*

![](../images/week02/GIMPmod3a.png)  
*Match the rectangle between the other pads and then select the original rectangle on the 'wrong bridge'*

![](../images/week02/GIMPmod4a.png)  
*Use the fill tool to switch that 'wrong bridge' to black*


---

##Job1: 2D vector of final project

I used Inkscape with this task, as I'm quite familiar with it. I didn't bother with exact scale and design as I was thinking of just drawing a basic concept images of what I was after.

I began by selecting *circle* -tool and with *CTRL* pressed down, I draw a circle. then I copied the circle and laid it on top of the first one. Then I used *transform* -tool to make 2nd circle +10% bigger. Now I had outer frame of the pocketwatch.

Next I used *group* on both circles to group them together and made a copy next outerframe. This will represent the lid of the pocketwatch.

I added rectangles onto both frames as hinges and smaller rectangles as pieces of the latch. I used *align & distribute* -tool to match them to each other positionwise. For the lid, I used *spiral* -tool to mimic concave inside of the lid.

Biggest trouble I had, was with the small circles and their distribution along inside circle. After few trial&errors and even one Inkscape-crash, I decided just to use *guidelines* (click and drag from top or side measurement-bars to enable guidelines, they didn't stay in view for screenshot) and good'ol eyeball to align them to cross each other in middle of the clockface. Then, using those guides as a helper, I made the circles to match 12h clockface.

I have to research later how to do it easier, but for now, this will suffice.

![](../images/week02/concept.jpg "clockface of pocketwatch")  
*Clockface of pocketwatch*

Next I made a side profile image by doing double rectangles next to each other as the frame. Then I drew two ellipses beginning from top middle corner of the frame, one as the lid, second as backside. This worked fine when you keep the frame rectangles as the layer above the ellipses.

Then I made the lid using my previous design (the clockface concept), by copying frame with one hinge part. Then using layers, I hid part of the hinge underneath the lid. The lid would have my initials in some fancier style. Here I just used CommercialScript BT for it, depends on the casting process what kind of style I will finally use.

![](../images/week02/concept2wip2.jpg "ellipses for lids before hiding half of them")  
*Ellipses for lids before hiding half of them*

Next I copied the E from my initials, then bend and resized it to match the side profile picture to bring some perspective to image. (In hindsight, I should have used the brassish colour on that side first, and then attach the E in there.)

Just a quick colour filling to both images to match my project idea.

![](../images/week02/concept2.jpg "side and top of pocketwatch")  
*Side and top of pocketwatch*

---

## Job2: 3D model of final project

So I used Fusion360 to design the chassis of my pocket watch for my final project.

80mm in diameter (so it would fit in my waistcoat pocket), two-part design with hinge on one side and latch/screw on the other side.

I need to adjust this design when rest of the parts are designed and closer to machining it.

![](../images/week02/taskunauris_v1_v4.jpg)  
*Chassis as assembled. Hinge on the right and latch in left*

![](../images/week02/taskunauris_v1_v4b31.jpg)  
*Bottom/backside of chassis. Strengthening parts for hinge/latch are nicely visible*

![](../images/week02/taskunauris_v1_v4b32.jpg)  
*Top/frontside of chassis*

![](../images/week02/taskunauris_v1_v5_open.jpg)  
*Hinge open. TODO possibly animated*

I went by and tried different methods and tools to get something I liked, so some of the steps will not be documented as well as I would want, a little problem of just going with the flow.

Most of the parts are made with simple sketch and extrude.

  *Sketch* a shape.
  *Extrude* (shortcut *E*) the shape to thickness you want.

So, my initial parameters are:
  Outer diameter = 80mm
  Inner diameter = 70mm
  Thickness = 10mm
  Screw extrusion = 20mm
  Screw hole thickness = 6mm

![](../images/week02/pw1_sketch.jpg)  
*Initial sketch*

I began with initial sketch of overall shape in 2D with blocks as hinge and latch to work on them as I go along.

![](../images/week02/pw1_blocks2.jpg)  
*Hinge-side blocks done, with enough room to fit cylindrical hinge in the middle*

![](../images/week02/pw1_blocks4.jpg)  
*Latch-side blocks mostly done, here I used curve of the chassis as one constraint*

These initial blocks are useful to have later on when reinforcing is needed as I noticed how thin the chassis would become, especially on hinge-part.

![](../images/week02/pw2_c1.jpg)  
*Extruded bottom ring-part*

![](../images/week02/pw2_c3.jpg)  
*Added top ring-part*

![](../images/week02/pw2_c4.jpg)  
*Combined latch-parts into each ring*

![](../images/week02/pw3_h1.jpg)  
*Began matching holes for both hinge and latch*

![](../images/week02/pw3_h2.jpg)  
*Made holes as cylinder-bodies for hinges and cutting tools*

Around here, I chamfered lots of sharp corners, mostly of latch-part.

![](../images/week02/pw3_h3.jpg)  
*Hinge in place, now needs some room. Also latch has been chamfered and tapped*

![](../images/week02/pw3_h4.jpg)  
*Copied outer hinge-cylinders and added them to ends of the hinge to make bigger tool for room-making*

![](../images/week02/pw3_h5.jpg)  
*Here I used those extra cylinders to make cuts in to both rings*

As the pocketwatch will have some electronics inside, I needed to make some room inside. So I hollowed out the rings to 1mm thickness, but I also reinforced both hinge and latch sides.

![](../images/week02/pw4_h1.jpg)  
*Time to hollow out those rings, re-used those blocks from earlier phases to make reinforcements inside*

![](../images/week02/pw4_h3.jpg)  
*Latch-side reinforcing seen here, more material added inside*

![](../images/week02/pw4_h4.jpg)  
*Both top and bottom reinforced*

---
### Extra: Lid and bottom template

As extra I added my lid design from molding&casting-week.

![](../images/week02/pw5_l1.jpg)  
*Lid design added on top*

![](../images/week02/pw5_l3.jpg)  
*Sketched and extruded a block to house lid-hinge into top-ring*

Lid's hinge will be 'L'-shaped, so it's housing needs some adjustment.

![](../images/week02/pw6_l3.jpg)  
*Hollowed out hinge-housing*

Originally I made the block a bit too big, but after some matching with the hinge, I fixed it. Next I chamfered sharp corners from it and tapped a hole through the housing.

![](../images/week02/pw6_l4.jpg)  
*Hinge-housing cleaned up and tapped through*

Now I added my basic idea as the bottom-lid for the pocketwatch. As in my sketch up, I want to make the bottom from stone that I have, but just in case, I made a quick design with wooden bottom, which could be raster-decorated in lasercutter.

![](../images/week02/pw7_b1.jpg)  
*Initial sketch of bottom part*

Here I made sketch for both inner part and outer ring as I want to round the edge of the stone.

![](../images/week02/pw7_b2.jpg)  
*Inner extrude*

![](../images/week02/pw7_b3.jpg)  
*Outer extrude*

![](../images/week02/pw7_b4.jpg)  
*Rounding outer edge*

![](../images/week02/pw8_b5.jpg)  
*Wooden bottom plate*

Designs for wooden plate is still work-in-progress.

---

### The greater model of my pocketwatch

I added the lids from casting-mould project to the model of chassis.

I used different colours to indicate usage of different materials.

![](../images/week02/tnv1_7aa.jpg "Lids joined to chassis")  
*Lids joined to chassis*

![](../images/week02/tnv1_7bb.jpg "Hinges")  
*Hinges*

![](../images/week02/tnv1_7cc.jpg "Red stone bottom lid")  
*Bottom lid of red stone*


---

## Tool details:

### Alignment & distribution -tools in Inkscape

Very useful tools for aligning your images and vectors in *Inkscape*. With *Shift+Ctrl+A* you can open it's toolbox. (other toolbox-shortcuts in picture below)

![](../images/week02/inkscape_distr1.jpg "Shift + Ctrl + A")  
*A&D-tools, quick key: Shift + Ctrl + A*

You can see tooltips for each function by hovering your cursor over them, but I think they could be better, but trial and error is not an issue, as you can still undo your missteps.

With alignment, it's important to remember to select the right way of relations from drop-down menu, as seen below. Ones that I tend to use are *first selected* and *biggest object* and they are like they are written.

![](../images/week02/inkscape_distr.jpg "Relations are important")  
*Relations are important*

---

## Troubleshoot: Where are my local files of Fusion360 (if it stops working) in Windows OS?

On the hard drive, locally cached files are stored in the below directories.

C:\Users\...\AppData\Local\Autodesk\Autodesk Fusion 360\XYZ123\W.Login.

XYZ123 = Different user codes are used as folder names.


In the W.Login directory you will find several folders.

F - this is the locally cached files stored on your system.
Q - this is the directory where files waiting to be uploaded are stored, also known as the "upload queue".

## Troubleshoot: Login error in Fusion360?

Check this [*solution*](https://knowledge.autodesk.com/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/Sign-in-Failure-There-was-an-error-logging-in-when-launching-Fusion-360-after-changing-timezones.html) from Autodesk.

---

##Thoughts and notes:

Fusion360 using cloudsaving for projects has some benefits, but as I constructed this documentation I noticed that couldn't access all my files locally (only one that was last worked on), as I didn't get Fusion360 installed on my main-PC. Now that I finally got access to cloudsaves, I made backups as .step -files, so I can open them in other CAD-programs.

Also, it seems that cloudsyncing has some issues as I had login error, that had direct support link, but it didn't really help. So I had to find the fix manually in the support database. I included it to troubleshooting above.


---

## Files:

[Step file of pocketwatch](../files/taskunauris_v1_v7.step)

[Fusion360 file of pocketwatch](https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/files/taskunauris%20v1%20v7.f3d)

[SVG file of concept1](https://gitlab.com/eemiltry/students_template_site/-/raw/master/docs/files/Concept.svg)

[SVG file of concept2](../files/concept2.svg)

[GIMP file of layout](../files/gimplayout.png)

[GIMP file of modified layout](../files/GIMPmoddedlayout.png)

---
