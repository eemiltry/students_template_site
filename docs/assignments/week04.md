# 4. Computer controlled cutting

This week I was introduced to two different cutting machines, laser- and vinylcutter. Behnaz had made us basic raster and vector tasks to run on Epilog Fusion CO2-lasercutters, so as a group work, we tried out different settings for those tasks.
Lasercutters use four basic parameters, SPEED, POWER, FREQUENCY, DPI (dots per inch).
Speed and power affect both vectoring- and rastering-modes, frequency affects vectoring and DPI affects rastering.

By adjusting those according to what material we are cutting/rastering we should have close to perfect operations. There are common settings for different materials in the Epilog's "printing" interface that have been found to work for that specific material.

##Groupwork, Vector and Raster 3mm MDF, examining kerf:

![](../images/week03_cutting/DSC_0326.jpg "setting up our vector cutting")  
*Setting up our vector cutting*

First we used the Epilog Fusion M2 (75W) to make vector cuts. Basic setup for Epilog is that you check it's ON, check that both airvalve and pumpmotor are on, then we put our material in the machine. For normal use, it's better to place the MDF-board against the rails, so if need be, it's easier to replace it on same position as previously. Now we need to check the focus for laser with placing the focus-triangle-tool on the laser-head and adjust the height of z-table so that triangle slightly touches the material, e.g. lift table until triangle begins to lift off from it's hooks and then lower the table a bit, set the focus by pressing the joystick in. Next we jog the laserhead into our chosen origin point and again set it by pressing the joystick in.
There could have been a more advanced technique for doing this, but we first run the text as a raster and then made individual squares with specified parameters.

### Vectoring

![](../images/week03_cutting/DSC_0327.jpg "First the text, then the work")  
*First the text, then the work*

First row was done with static Power 100 and Frequency 20, and varying Speed 20 -> 60

Second row has static Speed 20 and Frequency 20, and varying Power 100 -> 30

Third row has static Speed 50 and Power 100, and varying Frequency 60 -> 10

Varying the Speed means how long the laser stays in one place, longer time = more cutting time, so on first row, S20 and S30 cut all the way through.

Varying the Power means how strong is the laser beam, so P50 = half power, laser didn't cut through on last two squares.

Varying the Frequency means how intense is the pulse of the laser. With S50 P100, we didn't cut through any squares, except F20, which we accidentally run twice.

Also we noticed jitter on s50p100f30 and-f60 at start of cut, and informed the staff about that, who later run some tests about it and noted this "feature".

![](../images/week03_cutting/DSC_0329.jpg "final vector cuts on MDF")  
*Final vector cuts on MDF*

### Rastering

Second we used the Epilog-mini to make rastering. Basic setup differs a bit from the Larger Epilog, Origin is set by hand after releasing axis locks and focus finding is integrated to printing settings via Autofocus-tab. And again we made this square by square.

First row: Power 40, DPI 300, Speed 50->10

Second row: Speed 50, DPI 300, Power 90->10

Third row: Speed 50, Power 40, DPI 75->600

Speed in raster can cause burning/melting of material, if laserbeam goes too slowly in a raster-sweep. This is easily seen on first row with Speed 20 and 10 in picture below.

Power in raster means how deep the rastering goes into material.

DPI in raster means the quality of your raster, how detailed is the image that laser burns.

NOTE: First square was rastered accindentally in 600 dpi, and rough edge on MDF-board might have caused some focus issues as the board couldn't be flush with the sides of cutting area.

![](../images/week03_cutting/DSC_0331.jpg "Epilog-mini rastering on MDF")  
*Epilog-mini rastering on MDF*

![](../images/week03_cutting/DSC_0332.jpg "rough edge of MDF")  
*Rough edge causing a bit of warp in MDF*

![](../images/week03_cutting/DSC_0334.jpg "finished test-piece")  
*Test-piece finished*

Next we were to examine kerf of the Epilog Fusion M2 (75W). With our tutor, we made a design that would show us the kerf on the bigger lasercutter.

![](../images/week03_cutting/DSC_0336.jpg "designing with our tutor")  
*Designing kerf test with our tutor*

![](../images/week03_cutting/DSC_0337.jpg "testing kerf with this do-hickey")  
*Testing kerf with this do-hickey*

Next our tutor had a demo about what happens if laserhead is not focused correctly to cutting surface. If focus is set too high, laser won't cut through material and it burns the material more. If focus is set too low, laser cuts straight through and raster goes deeper.

![](../images/week03_cutting/DSC_0340.jpg "wrong focus demo in the works")  
*Wrong focus -demo in the works*

![](../images/week03_cutting/DSC_0341.jpg "example of too high/low focus")  
*Example of too high or low focus*


After examining properties of lasers we moved on to the *vinylcutter* (Roland CAMM-1 GS-24). Tutor showed us the basic order how to setup the machine for cutting properly. This procedure will be covered in detail in next section when I'm doing my own cutting job.

![](../images/week03_cutting/DSC_0342.jpg "hard-to-see cut on blue vinyl")  
*Subtle cuts on blue vinyl*

When the cut edges are hard to see, vinylcutter has done it's job properly.

![](../images/week03_cutting/DSC_0343.jpg "the same cut removed from vinyl")  
*The same cut removed from vinyl*



## JOB1: use the vinylcutter

After seeing pink vinyl in the lab storage, I wanted to replicate a picture of a bunny from a plate and mug that I have had since I was 5 years old I think.

![](../images/week03_cutting/DSC_0369.jpg "bunny plate")  
*Bunny plate*

I began the design with image above, by importing it to Inkscape. I cropped image with a rectangular mask to get a close up of just the bunny in picture.

Next I traced bitmap of the whole bunny and manually adjusted vectorlines on the edges of colours, as they were quite hazy for the tracing tool. I made different layers for different colours of the bunny, thus managing different cut-designs for vinylcutter. First I would do the pink as it's the biggest.

![](../images/week03_cutting/bunny1.jpg "Whole bunny traced")  
*Whole bunny traced, each color divided to own layers*

As seen above, I traced different colours to different layers, so that I could use them individually when I switched to different coloured material. (In hindsight, should have arranged the small parts next to each other to save material, but luckily I was working with scrap pieces of material, so material loss was minimal)

On some parts it was necessary to edit the nodes for better lines. Editing nodes also allowed to adjust curvature of the lines, which was mandatory to fix the ears of the bunny, as their colours were clipping a bit.

Below you can see the "non-pink parts" aka. the details of the picture, traced in Inkscape. Some of them were too close to each other or too small to be cut that nicely. (Also fixed the other hindleg before I went to cutting.)

![](../images/week03_cutting/bunny2.jpg "Only non-pink traces shown")  
*Only non-pink traces shown*

First I set the material into the vinylcutter, and left some material out for the rollers to grab onto.

![](../images/week03_cutting/vinyl_setup1.jpg "adjusting vinyl into the cutter")  
*Adjusting vinyl into the cutter*

Measured the material with vinylcutter, by selecting edge detection, so the machine uses it's sensors to measure the width of the material and showing the measurement on the screen, as seen below.

![](../images/week03_cutting/vinyl_setup2.jpg "selecting measurement method as edge")  
*Selecting measurement method as edge*

![](../images/week03_cutting/vinyl_setup3.jpg "width of material")  
*After measuring, the machine shows width of material*

After adjusting the cutting piece to design origin, I needed to set origin for the machine.

![](../images/week03_cutting/vinyl_setup4.jpg "origin set as shown by tutor")  
*Origin set as shown by tutor*

You could use the machine (Roland CAMM-1 GS-24) just by ordering a print from Inkscape, but remember to set "printer" as vinylcutter and use measurements from the machine.

![](../images/week03_cutting/vinylcut1.jpg "just print it already, but check it first")  
*Just press print, BUT check it first*

![](../images/week03_cutting/vinylcut3.jpg "cut-cut-cut-cut")  
*cut-cut-cut-cut...*

The smallest bits didn't cut that cleanly, but they were kinda ok.

![](../images/week03_cutting/vinylcut2.jpg "some rough cuts")  
*Some rough cuts*

I used scissors to cut my design from material and tweezers to pick excess vinyl from design, BUT remember to pull along material, NOT up from material.

![](../images/week03_cutting/DSC_0371.jpg "vinyl is cut, time to remove excess material")  
*Vinyl is cut, time to remove excess material*

![](../images/week03_cutting/DSC_0372.jpg "excess removed, we have the pink bunny")  
*Excess vinyl removed, we have the pink bunny*

Now I have a pink bunny, next I need to cut other pieces for the bunny.

For the other coloured pieces of my design, I had to fight with scrap pieces of vinyl. First the machine didn't want to measure the pieces, then when it measured it, measurements were wrong. When I finally got it working, I understood that I could have made the parts, that I needed, closer together and easier for machine to cut from these scrap pieces. Oh well, good to know for the next time.

After cutting the small pieces, it was time for some tweezer-work to add them into only-pink-bunny. As I was speculating, some of the tiniest brown pieces didn't cut that well, like foreleg toe-lines, so I decided not to try again for those tiny pieces as they were not that important for the sticker.

![](../images/week03_cutting/DSC_0373.jpg "after some 'tweaking'")  
*After some 'tweaking'*

![](../images/week03_cutting/DSC_0374_1.jpg "finished tweaking")  
*Finished tweaking*

After the whole thing was adjusted, tweaked and done, it was time to stick it to something. In this case the target was my wife-to-be's laptop.

![](../images/week03_cutting/DSC_0375_2.jpg "bunny on the laptop")  
*Bunny on the laptop*

And now next to the original plate.

![](../images/week03_cutting/DSC_0376_2.jpg "TA-DAA!!!")  
*TA-DAA!!!*

### Conclusions:

-group up your smaller pieces to save material

-avoid too small details for your vinylcutters knife (perhaps there's options for changing different blades for the machine)

-using scrap pieces of material might mean a lot more work in adjusting the vinylcutter

-in Inkscape you can zoom in, a lot, to make fine details

-tracing lines of hand-painted and/or old/worn images require more manual adjustments than sharp and new raster images

-vinylcutter is a very nice tool if you plan properly and are patient with it


## JOB2: Make a construction kit

For this one, I used Fusion360 and parametric-design to do the pieces for construction kit. I went with hexagonal design with slots in the middle of each straight edge. Hexes should be 50mm in size.

To start with, I chose some parameters for my design:

  -material thickness, too thin it's flimsy, too thick bad joints

  -kerf, to take material removal into our equation, laser isn't magic

  -slot depth, should be in relation to whole size of piece

These in mind, I set to work.

![](../images/week03_cutting/conskit_params.jpg "parameters for lasercut-construction kit")  
*Parameters for lasercut-construction kit*

As seen above, I began by deciding my parameters to design from. First important parameter is the thickness of material as it has impact on other parameters. Taking lasercutting kerf into parameters is perhaps the most important parameter and as my parameters show, it's used function-like in my other parameters to take kerf into account.  
As lasercutter's kerf removes extra material from slots, I made my parameters based on this. Thus I make the slot kerf-size smaller than thickness of material, so after cutting, slot width would be the same as thickness of material.

List of used parameters:
  Thickness of material = 3mm
  Kerf of lasercutter on material = 0.4mm
  Slot width = thickness of material - kerf
  Slot depth = 7mm - kerf
  Hex size = 50mm + kerf
  Chamfer of slot = width of slot / 2
  Not actually used:
  Curve of slot = 3mm (I used this for measurement and also tried to make rounded chamfer)
  Hex B size = 30mm + kerf (I made another test with this, but felt a bit too small to handle)

![](../images/week03_cutting/conskit_sketch.jpg "sketch of a piece")  
*Sketch of a piece*

I used constraints to fix slots into midway of their side of the hexagon and to fix their position to each other.

To make connection easier, I chamfered slot entrances.

![](../images/week03_cutting/conskit2.jpg "changing straight corner")  
*Changing straight corner...*

Above I'm designing camfer for a slot corner, below I have set the chamfer ready to fix slot-corner.

![](../images/week03_cutting/conskit3.jpg "to chamfer")  
*... to chamfer*

Below: Simple image of a whole piece which shows chamfer midpoints for each slot-corner.

![](../images/week03_cutting/conskit_v4.jpg "a piece ready for matrix")  
*A piece ready for matrix*

Below is the finished model of a construction kit piece.

![](../images/week03_cutting/conskit_model.jpg "model of a piece")  
*Model of a piece*

And here is the arrangement of pieces, ready for lasercut. At this point I was very tired, so I just copy&pasted twelve pieces to get this thing to the lasercutter, so they are not fully aligned into matrix. Most likely there is a better (and even quicker) way to make a design into matrix in Fusion360.


![](../images/week03_cutting/conskit4.jpg "arrangement for lasercutter")  
*Arrangement (or matrix) for lasercutter*

After designing construction kit, I decided to make a prototype from cardboard.

I'm glad I did, as there was an issue when I exported my design from Fusion360 as pdf, it scaled somewhat wrongly and the pieces would have been about double the size I wanted. So I had to use Inkscape at lasercutter to rescale and remeasure my design before using lasercutter. Now I managed to get my cardboard prototypes like I designed them, but the material felt a bit too flimsy to keep the slots together, maybe I should have changed my kerf-parameter a bit for cardboard.

  Addendum: Fusion360 has drawing scale that was mostlikely the culprit for double size in pdf. Should have exported the drawing as *.dxf*, that would allow to open it in Inkscape and just change line width for lasercutting.

So I made a couple of kit-pieces from 3mm MDF as there was a scrap piece of it about the right size. So I adjusted my kerf-parameter of my design to work with lasercut-MDF and I have to say, the joint is just right. Tight enough to hold firm, but still easy enough to connect/disconnect.

![](../images/week03_cutting/conskit1.jpg "prototype cardboard in ring with two mdf-pairs, one inside ring and others left of ring")  
*Prototype cardboard pieces formed into a ring and two MDF-piece pairs, one pair inside of ring and other pair left of ring*

### Conclusions:

-don't do design work when you are already tired, you can get some small things wrong too easily

-take screenshots from your design-phases for easier documentation

-cloud-based projects are nice if they synchronize properly


---
## Troubleshoot: Where are my local files of Fusion360 in Windows OS?

On the hard drive, locally cached files are stored in the below directories.

C:\Users\...\AppData\Local\Autodesk\Autodesk Fusion 360\...\W.Login.

In the W.Login directory you will find several folders.

    F - this is the locally cached files stored on your system.
    Q - this is the directory where files waiting to be uploaded are stored, also known as the "upload queue".

## Troubleshoot: Login error in Fusion360?

Check this [*solution*](https://knowledge.autodesk.com/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/Sign-in-Failure-There-was-an-error-logging-in-when-launching-Fusion-360-after-changing-timezones.html) from Autodesk.

---
## Files:

[SVG-file of bunny](../files/pupu.svg)

[PDF of Conskit](../files/conskit_v1.pdf)

[PDF of Conskit in landscape](../files/conskit_v2_landscape.pdf)

[F3D file of Conskit](../files/conskit_v8.f3d)

[stl file of Conskit](../files/conskit_v8.stl)

[STEP file of Conskit](../files/conskit_v8.step)

---
