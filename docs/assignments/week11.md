# 11. Input devices

assignment
   individual assignment:
      measure something: add a sensor to a microcontroller board
      that you have designed and read it
   group assignment:
      probe an input device's analog levels and digital signals

## Design parameters

By tutors advice, I made both input and output device on one PCB, having their own connectors.

I made a simple phototransistor ciruit as my input device. I'm thinking of using a phototransistor in my final project to detect if a lid is shut or not, to control LEDs on/off. Other possibility would be magnetic sensor, other passive sensor, or just a microswitch.

I based my design around [input devices -week](http://academy.cba.mit.edu/classes/input_devices/index.html) [hello.light.45](http://academy.cba.mit.edu/classes/input_devices/light/hello.light.45.png) -design, with [PT15-21C/TR8](https://www.digikey.com/product-detail/en/everlight-electronics-co-ltd/PT15-21C-TR8/1080-1380-1-ND) -*phototransistor*, *drain-resistor* of 10k ohms, *filter-capacitor* of 1uF and 6 pin connector FTDI-style.

![](../images/week12/debug0.png)  
*KiCAD view of my PCB layout, <-output input->*

## Testing input devices

TODO: test setup, pictures, code, etc.


__OLD STUFF__

![](../images/week11/input0.jpg)  
*Input board after cutting outline*

![](../images/week11/input1.jpg)  
*Input board ready for components*

![](../images/week11/input2.jpg)  
*Board ready*
